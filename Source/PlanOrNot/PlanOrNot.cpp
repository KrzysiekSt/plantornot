// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlanOrNot.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PlanOrNot, "PlanOrNot" );

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlanOrNotGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PLANORNOT_API APlanOrNotGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

#pragma once
#include "./GameActor.h"

class Weapon
{
public:
    virtual ~Weapon();
    virtual void affect(GameActor actor) = 0;
};

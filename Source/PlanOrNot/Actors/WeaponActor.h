// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponActor.generated.h"

UCLASS()
class PLANORNOT_API AWeaponActor : public AActor
{
	GENERATED_BODY()
	
public:
	AWeaponActor();

	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* skeletal_mesh;

	UPROPERTY(EditAnywhere)
	double velocity_factor;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	void fire(FTransform camera);
};

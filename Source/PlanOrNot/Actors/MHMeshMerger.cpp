#include "MHMeshMerger.h"

#include "SkeletalMeshMerge.h"

// Sets default values for this component's properties
UMHMeshMerger::UMHMeshMerger()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UMHMeshMerger::BeginPlay()
{
	Super::BeginPlay();
	
	AActor* parentActor = this->GetOwner();
	AHuman* parentHuman = Cast<AHuman>(parentActor);

	skeletalMesh = mergeMeshes();
	parentHuman->GetMesh()->SetSkeletalMesh(skeletalMesh);

	if (parentHuman->IsLocallyControlled())
	{
		parentHuman->GetMesh()->SetRelativeLocation(MeshLocationOffset);
		parentHuman->GetMesh()->SetRelativeRotation(MeshRotationOffset);
	}
}

void UMHMeshMerger::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

USkeletalMesh* UMHMeshMerger::mergeMeshes()
{
	TArray<USkeletalMesh*> meshArrayCopy = MeshArray;

	if (meshArrayCopy.Num() <= 1)
	{
		UE_LOG(LogTemp, Error, TEXT("Merging requires more than one Skeletal Mesh!"))
		return nullptr;
	}

	USkeletalMesh* mesh = NewObject<USkeletalMesh>();
	const int top_LOD_cut = 0;

	FSkeletalMeshMerge merger(mesh, meshArrayCopy, TArray<FSkelMeshMergeSectionMapping>(), top_LOD_cut, EMeshBufferAccess::ForceCPUAndGPU);

	if (!merger.DoMerge())
	{
		UE_LOG(LogTemp, Error, TEXT("Skeletal mesh merge failed!"));
		return nullptr;
	}

	if (Skeleton)
	{
		mesh->SetSkeleton(Skeleton);
	}

	return mesh;
}
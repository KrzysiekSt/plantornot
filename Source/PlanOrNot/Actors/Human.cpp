// Fill out your copyright notice in the Description page of Project Settings.
#include "Human.h"

#include "SkeletalMeshMerge.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Engine/SkeletalMesh.h"
#include "Animation/Skeleton.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
AHuman::AHuman()
{
    PrimaryActorTick.bCanEverTick = true;
    camera = CreateDefaultSubobject<UCameraComponent>(TEXT("camera"));
    camera->SetupAttachment(RootComponent);
    camera->bUsePawnControlRotation = true;
    camera->SetUsingAbsoluteRotation(false);
}

// Called when the game starts or when spawned
void AHuman::BeginPlay()
{
    Super::BeginPlay();
    camera->SetRelativeLocation(CAMERA_OFFSET);
    camera->SetRelativeRotation(CAMERA_ROTATION);
    spawnWeapon();
}

void AHuman::spawnWeapon()
{
    UBlueprint* blueprint = LoadObject<UBlueprint>(NULL, TEXT("/Game/AK_BP"));
    
    if (blueprint)
    {
        UClass* ak_class = blueprint->GeneratedClass;
        const FName WEAPON_SOCKET_NAME = "middle_01_r";
        FTransform weapon_socket_transform = GetMesh()->GetSocketTransform(WEAPON_SOCKET_NAME, RTS_Component);
        FRotator proper_rotation{0,180,0};
        auto actor = GetWorld()->SpawnActor(ak_class, &weapon_socket_transform);
        weapon = Cast<AWeaponActor>(actor);
        if(weapon)
        {
            weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WEAPON_SOCKET_NAME);
            weapon->AddActorLocalRotation(proper_rotation);
        }
    }
}

void AHuman::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AHuman::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAction("Attack", EInputEvent::IE_Pressed, this, &AHuman::attack);

    PlayerInputComponent->BindAxis("MoveForward", this, &AHuman::moveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AHuman::moveRight);

    PlayerInputComponent->BindAxis("CameraPitch", this, &AHuman::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("CameraYaw", this, &AHuman::AddControllerYawInput);
    
    PlayerInputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AHuman::Jump);
    PlayerInputComponent->BindAction("Crouch", EInputEvent::IE_Pressed, this, &AHuman::crouchPressed);
    PlayerInputComponent->BindAction("Crouch", EInputEvent::IE_Released, this, &AHuman::crouchReleased);
}

void AHuman::attack()
{
    weapon->fire(camera->GetComponentTransform());
}

void AHuman::moveForward(float inputValue)
{
    AddMovementInput(GetActorForwardVector() * inputValue);
}

void AHuman::moveRight(float inputValue)
{
    AddMovementInput(GetActorRightVector() * inputValue);
}

void AHuman::crouchPressed()
{
    if (!isCrouching && CanCrouch())
    {
        Crouch();
        isCrouching = true;
        return;
    }
    if (isCrouching && toggleCrouch)
    {
        UnCrouch();
        isCrouching = false;
    }
}

void AHuman::crouchReleased()
{
    if (isCrouching && !toggleCrouch)
    {
        UnCrouch();
        isCrouching = false;
    }
}
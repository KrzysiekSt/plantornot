#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "SkeletalMeshMerge.h"
#include "Engine/SkeletalMeshSocket.h"
#include "WeaponActor.h"
#include "Engine/SkeletalMesh.h"
#include "Animation/Skeleton.h"

#include "Camera/CameraComponent.h"
#include "./HumanSubclasses/MovementController.h"
#include "../Interfaces/Weapon.h"
#include "../Interfaces/GameActor.h"
#include "GameFramework/SpringArmComponent.h"

#include "Human.generated.h"

UCLASS()
class PLANORNOT_API AHuman : public ACharacter
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, Category="Camera offset settings")
    const FVector CAMERA_OFFSET {30,0,60};

    UPROPERTY(EditAnywhere, Category="Camera offset settings")
    const FRotator CAMERA_ROTATION{0, 0, 0};

    UPROPERTY(EditAnywhere, Category="Input options")
    bool toggleCrouch = false;

    AHuman();

    // Called every frame
    virtual void Tick(float DeltaTime) override;
    
    // for PlayableHuman
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:

private:
    
    virtual void BeginPlay() override;

    void moveForward(float inputValue);
    void moveRight(float inputValue);

    bool isCrouching = false;
    void crouchPressed();
    void crouchReleased();
    void spawnWeapon();
    void attack();

    UPROPERTY(VisibleAnywhere)
    UCameraComponent* camera;

    int health;
    int body_armour;
    int price;
    bool helmet;

    TUniquePtr<MovementController> movement_controller;
    AWeaponActor* weapon;
};

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Human.h"
#include "MHMeshMerger.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PLANORNOT_API UMHMeshMerger : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMHMeshMerger();

	UPROPERTY(EditAnywhere, Category= "MH Components")
	TArray<USkeletalMesh*> MeshArray;

	UPROPERTY(EditAnywhere, Category="MH Components")
	USkeleton* Skeleton;

	UPROPERTY(EditAnywhere, Category="MH Mesh offsets")
	FVector MeshLocationOffset {0, 0, -90};
	
	UPROPERTY(EditAnywhere, Category="MH Mesh offsets")
	FRotator MeshRotationOffset {0, -90, 0};

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	USkeletalMesh* skeletalMesh;
	USkeletalMesh* mergeMeshes();
};

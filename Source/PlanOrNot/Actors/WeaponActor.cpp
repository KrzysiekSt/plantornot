// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponActor.h"

#include "ABulletActor.h"


// Sets default values
AWeaponActor::AWeaponActor()
{
	PrimaryActorTick.bCanEverTick = true;
	skeletal_mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("mesh"));
}

void AWeaponActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWeaponActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeaponActor::fire(FTransform camera)
{
	UBlueprint* bullet_blueprint = LoadObject<UBlueprint>(NULL, TEXT("/Game/AK_Bullet_BP"));
	if (bullet_blueprint)
	{
		UClass* ak_class = bullet_blueprint->GeneratedClass;
		
		FTransform offset = FTransform(20 * camera.TransformVector(FVector::ForwardVector));

		const FTransform new_transform = camera * offset;
		auto actor = GetWorld()->SpawnActor(ak_class, &new_transform);
		auto bullet = Cast<AABulletActor>(actor);
		if(bullet)
		{
			const FRotator proper_rotation{0,-90,0};
			bullet->AddActorLocalRotation(proper_rotation);
			const double FORCE = bullet->mass * velocity_factor * velocity_factor;
			auto component = bullet->GetComponentByClass(UStaticMeshComponent::StaticClass());
			UStaticMeshComponent* static_mesh_component = Cast<UStaticMeshComponent>(component);
			if(static_mesh_component)
			{
				static_mesh_component->AddImpulse(bullet->GetActorForwardVector() * FORCE);
			}
		}
	}
}


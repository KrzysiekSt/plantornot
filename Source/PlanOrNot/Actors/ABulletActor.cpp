// Fill out your copyright notice in the Description page of Project Settings.


#include "ABulletActor.h"

// Sets default values
AABulletActor::AABulletActor()
{
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));

}

void AABulletActor::BeginPlay()
{
	Super::BeginPlay();
	mesh->OnComponentBeginOverlap.AddDynamic(this, &AABulletActor::hit);
	
}

void AABulletActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AABulletActor::hit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GetOwner()->Destroy();
}


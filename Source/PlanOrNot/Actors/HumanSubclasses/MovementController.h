#pragma once

class MovementController
{
public:
    virtual ~MovementController();

    virtual void runForward() = 0;
    virtual void runBackward() = 0;
    virtual void runLeft() = 0;
    virtual void runRight() = 0;

    virtual void walkForward() = 0;
    virtual void walkBackward() = 0;
    virtual void walkLeft() = 0;
    virtual void walkRight() = 0;


    virtual void crouchForward() = 0;
    virtual void crouchBackward() = 0;
    virtual void crouchLeft() = 0;
    virtual void crouchRight() = 0;

    virtual void jump() = 0;

};

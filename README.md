Besic FPS game using Unreal Engine 5.1

The build:
- https://samszczecin-my.sharepoint.com/:f:/g/personal/28254_s_pm_szczecin_pl/Eox8WtWptaFFioLT-n7olI0BO8CZ8AV74qteY5ei6Y1wCw?e=bknYxi

Used content:
- https://www.unrealengine.com/marketplace/en-US/product/animation-starter-pack
- https://www.unrealengine.com/marketplace/en-US/product/fps-weapon-bundle
- https://www.mixamo.com
- https://www.unrealengine.com/en-US/metahuman
